//=========================================================================
// Configuration: HTTP Server settings

// Port. Default is most likely 80
var port = process.env.PORT || 3000;

// static folder to serve files from
var gameDirectory = __dirname + "/webgame";

//=========================================================================
// Configuration: Database

// Our MongoDB address, which includes the database name
var mongodbURL = process.env.MONGOHQ_URL || "mongodb://localhost/html5DemoGame";

//=========================================================================
// Configuration: Authentication

// Our Session secret
var sessionSecret = process.env.SESSION_SECRET || "SOME RANDOM STRING";

// Google authentication
var GOOGLE_RETURN_URL = process.env.GOOGLE_RETURN_URL || 'http://localhost:3000/auth/google/return';
var GOOGLE_REALM = process.env.GOOGLE_REALM ||'http://localhost:3000/';

// Facebook authentication requires a Facebook application
var FACEBOOK_APP_ID = process.env.FACEBOOK_APP_ID || "604710686209575";
var FACEBOOK_APP_SECRET = process.env.FACEBOOK_SECRET || "41a70fecd35f7692dc02fe9a18fd15ed";
var FACEBOOK_CALLBACK_URL = process.env.FACEBOOK_CALLBACK_URL || "http://localhost:3000/auth/facebook/callback";

//=========================================================================
// Load our dependencies
//
var express = require('express'),
	MongoClient = require('mongodb').MongoClient,
	passport = require('passport');

var LocalStrategy = require('passport-local').Strategy,
	GoogleStrategy = require('passport-google').Strategy,
	FacebookStrategy = require('passport-facebook').Strategy,
	TwitterStrategy = require('passport-twitter').Strategy;


//=========================================================================
// Create server
// Our app module is going to a customized express server
//
var app = express.createServer(
	//express.logger(),
	express.static( gameDirectory ),
	express.cookieParser(),
	express.bodyParser(),		// parse post data of the body
	express.session({ secret: sessionSecret }),
	passport.initialize(),
	passport.session()			// to support persistent login sessions (recommended).
);


// Views and Routes
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(app.router);

//=========================================================================
// Server Configuration
//

// Set different error handlers for different environments
// Default environment is development
app.configure('development', function(){
	app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
	app.use(express.errorHandler());
});

// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.  However, since this example does not
//   have a database of user records, the complete Google/Facebook/Twitter profile
//   is serialized and deserialized.
passport.serializeUser(function(user, done) {
	done(null, user);
});

passport.deserializeUser(function(obj, done) {
	done(null, obj);
});

//=========================================================================
// Ensuring Authenticated users
//
// Simple route middleware to ensure user is authenticated.
//   Use this route middleware on any resource that needs to be protected.  If
//   the request is authenticated (typically via a persistent login session),
//   the request will proceed.  Otherwise, the user will be redirected to the
//   login page.
function ensureAuthenticatedUser(req, res, next) {
	if (req.isAuthenticated()) { return next(); }

	// @TODO - nice to have feature:
	// We could, if we wanted, store the originally requested URL in the session
	// so we know where to return the user to later once they log in.
	// The trick is, in the /callback routes, we would need to check if a
	// redirectUrl exists and go to it instead of using the successRedirect property.
	// req.session.redirectUrl = req.url;

	console.log('User is not authenticated. Redirecting to login page');
	req.flash("warn", "You must be logged-in to do that.");
	res.redirect('/');
}

//*******************************************************
// Username & Password
// The most widely used way for websites to authenticate users is via a username
// and password. Support for this mechanism is provided by the passport-local module.
(function() {

	// Use the LocalStrategy within Passport.
	//   Strategies in passport require a `verify` function, which accept
	//   credentials (in this case, a username and password), and invoke a callback
	//   with a user object.
	passport.use(new LocalStrategy(
		function(username, password, done) {
			// Find the user by username.  If there is no user with the given
			// username, or the password is not correct, set the user to `false` to
			// indicate failure and set a flash message.  Otherwise, return the
			// authenticated `user`.
			console.log( "Attempting to authenticate local user: " + username );
			var userInfo = {username: username};
			dbMethods.findUser( userInfo, function(err, user) {
				if (err) {
					console.log( "Could not find existing user with the given username." );
					return done(err);
				}
				if (!user) {
					console.log( "Could not find existing user with the given username." );
					return done(null, false, { message: 'Unknown user ' + username });
				}

				console.log( "We found an existing user with the given username, verifying password!" );
				if (user.password != password) {
					console.log( "Invalid password!" );
					return done(null, false, { message: 'Invalid password' });
				}

				console.log( "Password matched! User authenticated!" );
				return done(null, user);
			});
		}
	));

	// POST /login
	//   Use passport.authenticate() as route middleware to authenticate the
	//   request.  If authentication fails, the user will be redirected back to the
	//   login page.  Otherwise, the primary route function function will be called,
	//   which, in this example, will redirect the user to the home page.
	//
	app.post('/',
		passport.authenticate('local', { failureRedirect: '/', successRedirect: '/', failureFlash: true }),
		function(req, res) {
			res.redirect('/');
	});

	app.get('/createAccount', function(req, res){
		res.render('createAccount', { user: req.user });
	});

	app.post('/createAccount', function(req, res) {
		var username = req.body.username;
		var password = req.body.password;
		var email = req.body.email;
		var givenName = req.body.givenName;
		var familyName = req.body.familyName;
		console.log( "Attempting to create new account: " + username );

		// @TODO, it would be nice to have a better form validation scheme
		if( username.length === 0 ) {
			console.log( "Create new account failed: Requires valid username" );
			res.redirect('createAccount');
			return;
		}
		if( password.length === 0 ) {
			console.log( "Create new account failed: Requires valid password" );
			res.redirect('createAccount');
			return;
		}
		if( email.length === 0 ) {
			console.log( "Create new account failed: Requires valid email" );
			res.redirect('createAccount');
			return;
		}
		if( givenName.length === 0 ) {
			console.log( "Create new account failed: Requires valid givenName" );
			res.redirect('createAccount');
			return;
		}
		if( familyName.length === 0 ) {
			console.log( "Create new account failed: Requires valid familyName" );
			res.redirect('createAccount');
			return;
		}

		var userInfo = { username: username };

		dbMethods.findUser( userInfo, function(err, user) {
			if( user ) {
				console.log( "Create new account failed: existing username" );
				res.redirect('createAccount');
				return;
			}

			// profile data
			userInfo.password = password;
			userInfo.email = email;
			userInfo.displayName = username;
			userInfo.name = { givenName: givenName, familyName: familyName };

			// game data
			userInfo.userScore = 0;

			dbMethods.createNewUser( userInfo, function(err, result) {
				console.log( "Create new account: SUCCESS" );
				//@TODO - it would be nice to have the user immediately authenticated
				// so they do not have to login
				return res.redirect("/");
			});
		});
	});
})();


//*******************************************************
// Facebook
//
// In order to use Facebook authentication, you must first create an app at
// Facebook Developers. When created, an app is assigned an App ID and App Secret.
// Your application must also implement a redirect URL, to which Facebook will
// redirect users after they have approved access for your application.
//
(function() {
	passport.use(new FacebookStrategy({
			clientID: FACEBOOK_APP_ID,
			clientSecret: FACEBOOK_APP_SECRET,
			callbackURL: FACEBOOK_CALLBACK_URL
		},
		function(accessToken, refreshToken, profile, done) {
			// We will attempt to find an existing user that is mapped to this Facebook account
			// otherwise we will need to create a new user in our database and then associate
			// it with this Facebook account.
			var userInfo = { facebookId: profile.id };
			dbMethods.findOrCreateUser( userInfo, profile, function(err, user) {
				return done(err, user);
			} );
		}
	));

	// Redirect the user to Facebook for authentication.  When complete,
	// Facebook will redirect the user back to the application at
	//     /auth/facebook/callback
	app.get('/auth/facebook', passport.authenticate('facebook'));

	// Facebook will redirect the user to this URL after approval.  Finish the
	// authentication process by attempting to obtain an access token.  If
	// access was granted, the user will be logged in.  Otherwise,
	// authentication has failed.
	app.get('/auth/facebook/callback', passport.authenticate('facebook', {
		successRedirect: '/',
		failureRedirect: '/'
	}));
})();

//*******************************************************
// Google
//
// When using Google for sign in, your application must implement a return URL,
// to which Google will redirect users after they have authenticated. The realm
// indicates the part of URL-space for which authentication is valid. Typically
// this will be the root URL of your website.
//
(function() {
	passport.use(new GoogleStrategy({
			returnURL: GOOGLE_RETURN_URL,
			realm: GOOGLE_REALM
		},
		function(identifier, profile, done) {
			// We will attempt to find an existing user that is mapped to this Google account
			// otherwise we will need to create a new user in our database and then associate
			// it with this google account.
			var userInfo = {openId: identifier};
			dbMethods.findOrCreateUser( userInfo, profile, function(err, user) {
				return done(err, user);
			} );
		}
	));

	// Two routes are required for Google authentication. The first route redirects
	// the user to Google. The second route is the URL to which Google will return
	// the user after they have signed in.

	// Redirect the user to Google for authentication.  When complete, Google
	// will redirect the user back to the application at
	//     /auth/google/return
	app.get('/auth/google', passport.authenticate('google', { failureRedirect: '/' }),
		function(req, res) {
			res.redirect('/');
		}
	);

	// Google will redirect the user to this URL after authentication.  Finish
	// the process by verifying the assertion.  If valid, the user will be
	// logged in.  Otherwise, authentication has failed.
	app.get('/auth/google/return', passport.authenticate('google', { failureRedirect: '/' }),
		function(req, res) {
			res.redirect('/');
		}
	);
})();


//=========================================================================
// Start the server!
//
app.listen(port, function(){
	console.log("Updated express server listening on port %d in %s mode", app.address().port, app.settings.env);
});


//=========================================================================
// Databases!
//

// Local storage for utility functions
var dbMethods = {};

// connect to the database
MongoClient.connect( mongodbURL, function( error, db ) {

	if( error ) {
		console.log( "MongoClient.connect: Could not connect to database: ");
		console.log( error );
		return;
	}
	console.log( "MongoClient.connect: Connected to our mongodb database. You should notice the connection in the mongod.exe logs." );

	// grab the "users" collection and generate a few utility functions
	db.collection('users', function(err, collection) {

		if( err ) {
			console.log( "MongoClient.db.collection: Could not connect to 'users' collection in the database" );
			console.log( err );
			return;
		}
		console.log( "MongoClient.db.collection: 'users' collection accessed, generating utility functions" );

		//=========================================================================
		// Utility function: finds an existing user
		//
		// userInfo could represent any number of user properties.
		// * openId for google users
		// * facebookId for facebook users
		// * username for local users
		dbMethods.findUser = function( selector, callback ) {
			collection.findOne( selector, callback );
		};

		//=========================================================================
		// Utility function: creates a new user
		//
		dbMethods.createNewUser = function( userInfo, callback ) {
			// If you attempt to insert a document without the _id field, it will add an _id field and populate the field with a unique ObjectId.
			collection.insert( userInfo, null, function(err, result) {
				// the object we passed in has been updated and should now include the _id property
				// We can ignore result, because where is the documentation that explains it?!
				if( err ) {
					callback(err, null);
				} else {
					callback(null, userInfo);
				}
			});
		};

		//=========================================================================
		// Utility function: Finds a user or creates a new one
		// Takes in extra profile data given by our third-party systems to add to
		// the local user data
		//
		dbMethods.findOrCreateUser = function( selector, thirdPartyProfileData, callback ) {
			collection.findOne( selector, function( err, result ) {
				// We found a record of this user in our database, so we will use that!
				if( result ) {
					console.log( "We found an existing user through the authentication method!" );
					callback( err, result );
				}
				else {
					// If no user was found, then we can create a new one and associate it
					// with the given authentication account so it can be looked up again later
					console.log( "Could not find existing user through third-party account. Attempting to creating a new one." );

					// Update our profile data with properties from the third-party profile so that
					// our local user data has them before inserting it into the database
					var userInfo = selector;
					userInfo.displayName = thirdPartyProfileData.displayName;
					userInfo.name = thirdPartyProfileData.name;
					userInfo.email = thirdPartyProfileData.emails[0];

					// game data
					userInfo.userScore = 0;

					collection.insert( userInfo, null, function(err, result) {
						if( err ) {
							console.log( "Error: Could not create new user from third-party account" );
							console.log( err );
							callback( err, null );
						}
						else {
							console.log( "New user created from third-party account!" );
							callback( null, userInfo );
						}
					});
				}
			}); // end collection.findOne()
		};	// end dbMethods.findOrCreateUser()

		//=========================================================================
		// Utility function: gets the top scores
		//
		dbMethods.topTenScores = function(callback) {
			// get the top 10 scored entries from our Users database
			// for each item returned, use dbMethods.findUser for the given user
			collection.find().sort({ userScore: -1 }).limit(10).toArray(function(error,results) {
				if( error ) {
					callback(false);
					return;
				}

				var output = [];
				for(var i in results) {
					output.push( {
						displayName: results[i].displayName,
						userScore: results[i].userScore,
						id: results[i]._id
					});
				}
				callback(output);
			});
		};

		//=========================================================================
		// Utility function: updates a players score
		//
		dbMethods.updateUserInfo = function(userInfo, callback) {
			if( !userInfo ) {
				callback(false);
				return;
			}

			//@TODO - make this work using .update() instead
			collection.save(userInfo, function(error, results) {
				if( error ) {
					console.log( "Could not update score for user" );
					callback( false );
				} else {
					console.log( "Score updated!", results );
					callback(results);
				}
			});
		};

		//=========================================================================
		// Utility function: clear the database
		// NOTE: You would not really want to make this accessible!!
		//
		dbMethods.clearUsers = function(callback) {
			collection.drop( callback );
		};

	}); // end db.collection for "users"

});	// end MongoClient.connect

//=========================================================================
// Game pages!
//
app.get('/', function(req, res){
	res.render('index', { user: req.user });
});

app.get('/account', ensureAuthenticatedUser, function(req, res){
	res.render('account', { user: req.user });
});

app.get('/logout', function(req, res){
	req.logout();
	res.redirect('/');
});

app.get('/game', ensureAuthenticatedUser, function(req,res) {
	console.log( "Going to our webgame" );
	res.render('game', { user: req.user });
});

app.post('/updateScore', ensureAuthenticatedUser, function(req,res) {
	//we get the data passed in from $.post() from the request.body
	req.user.userScore = req.body.userScore;

	console.log( "player score update requested with score: " + req.user.userScore );
	dbMethods.updateUserInfo( req.user, function(results) {
		console.log( "Player score updated? " + results );
	} );
});

app.get('/top-ten',function(req,res) {
	dbMethods.topTenScores(function(results) {
		res.json({ users: results });
	});
});

///////////////////////////////////
// Debug pages
///////////////////////////////////

app.get('/clearUsers',function(req,res) {
	console.log( "Clearing entire users database" );
	dbMethods.clearUsers(function(results) {
		res.json({ users: results });
	});
});