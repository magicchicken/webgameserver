/*
  Loader.js

  Manages loading of scripts and resources.
*/

//*****************************************************************************

var appLoader = (function() {

	//*****************************************************************************
	var EngineFiles = [

		// Required engine libraries
		"js_libraries/underscore-min.js",
		"js_libraries/Box2dWeb-2.1.a.3.min.js",

		// Core Engine Systems
		"js_engine/core/util.js",
		"js_engine/core/class.js",

		// Engine initial entry point, must come first
		"js_engine/engine.js",

		// Base Engine Systems
		"js_engine/evented.js",
		"js_engine/components.js",
		"js_engine/entity.js",
		"js_engine/canvas.js",
		"js_engine/assets.js",

		// Engine Modules
		"js_engine/module_input.js",
		"js_engine/module_sprites.js",
		"js_engine/module_scenes.js",
		"js_engine/module_physics.js",
		"js_engine/module_svg.js",
		"js_engine/module_anim.js",
		"js_engine/module_audio.js",
		"js_engine/module_tilemap.js",
		"js_engine/module_particles.js",

		"game/blockbreak/main.js"
	];

	//=========================================================================
	// function set_visibility(id, value) {
	// 	var e = document.getElementById(id);
	// 	e.style.display = value;
	// }

    //=========================================================================
	var getProgressFn;
	var $progress = $('#getLoadedProgress');

	function startProgressMeter( getProgress )
	{
		getProgressFn = getProgress;
		tickProgressMeter( );
	}

    //=========================================================================

	function tickProgressMeter( )
	{
		var progressIndicator = document.getElementById("progressIndicator");
		var pct = 100 * getProgressFn( );
		if ( pct < 100 )
		{
			$progress.width( pct );
			setTimeout( tickProgressMeter, 30 );
		}
		else
		{
			$progress.width( 100 );
			setTimeout( startGame, 300 );
		}
	}

	//=========================================================================

	function startGame( )
	{
		$('getLoadedText').hide();
		$('#getLoadedProgress').hide();
		//$('#pressSpace').show();
		$('#pressSpace').hide();

		$('#gameCanvas').focus();
		$(document).on('keyup.introSpace32', function(e) {
			if(e.which && e.which === 32) {
				$(document).off('keyup.introSpace32'); // turn off just this one keyup event
				myEngine.startNewGame();
				e.preventDefault();
			}
		});
	}

	//=========================================================================

	function loadEngine( additionalFiles )
	{
		var numResourcesLoaded = 0;
		var totalResources = EngineFiles.length;
		startProgressMeter( function() {
			if ( totalResources > 0 ) {
				return numResourcesLoaded / totalResources;
			} else {
				return 1;
			}
		});

		Modernizr.load([
			// {
				// test: navigator.appName.match(/Explorer/),
				// yep: "jquery.min.js",
				// nope: "zepto.min.js"
			// },
			{
				load: EngineFiles,

				callback: function( url, result, key )
				{
					console.log( "File loaded: ", url );
					++numResourcesLoaded;
				},

				complete: function()
				{
					console.log( "Game Engine Files loaded!");
				}
			},
			additionalFiles
		]);
	}


	//=========================================================================
	return {
		//set_visibility: set_visibility,
		startProgressMeter: startProgressMeter,
		tickProgressMeter: tickProgressMeter,
		loadEngine: loadEngine
	};

})();	// run immediately


//*****************************************************************************

// Let the HTML file run the app

//*****************************************************************************
