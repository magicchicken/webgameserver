//*****************************************************************************
// 2D Grid represents the playing field
//
// Required Systems:
// * Engine
// * Engine.Component
//
// Required libraries:
// *
// *
//
// Features:
// *
//
// @TODO:
// -
//*****************************************************************************

// Requires Engine namespace
(function() {
	engineAssert( Engine, "No Engine Definition" );
})();

//=========================================================================
// Tilemap Module
//=========================================================================
Engine.Tilemap = function() {

	var DebugFillStyle = "rgba(128, 0, 128, .4)";
	var DebugLineWidth = 1;
	var DebugStrokeStyle = "rgba(255, 0, 255, .4)";

	Engine.MapTile = Engine.Sprite.extend( {
		defaults: {
			row: 0,
			col: 0,
			hasCollision: false,
			debugDraw: false
		},

		draw: function(ctx) {
			this._super();

			// DEBUG DRAW
			if( this.properties.debugDraw ) {
				this.engine.setFont( "normal 12px Arial" );
				this.engine.fillText( this.properties.row + ", " + this.properties.col, this.properties.x, this.properties.y, "black", "center");

				var width = this.properties.width;
				var height = this.properties.height;
				var x = this.properties.x - width / 2;
				var y = this.properties.y - height / 2;
				this.engine.drawRect( x, y, this.tileWidth, this.tileHeight, DebugFillStyle, DebugLineWidth, DebugStrokeStyle );
			}
		}
	});


	//=========================================================================
	// TileLayer
	//
	// Must be a sprite because it needs to be to get added to our scene.
	// Scenes currently require sprites because it assumes there's a .step and a .draw
	// which are on the Sprite class
	//
	//@TODO, caculating the z for sorting this might require a better algorithm, or
	// the TileLayer can be in charge of rendering the tiles and skip adding them to the scene
	//
	// Extra stuff! Adding shadows using the cutegod asset set!
	// http://www.lostgarden.com/2007/05/cutegod-prototyping-challenge.html
	//=========================================================================
	Engine.TileLayer = Engine.Sprite.extend( {

		defaults: {
			mapData: null,
			tileData: null,
			debugDraw: false
		},

		init: function(props) {
			this._super(props);

			// initialize tiles, represented in a 2D array
			this.tiles = [];
			this.tileCount = 0;

			var tiles = this.properties.mapData.tiles;
			for( var i=0; i<tiles.length; ++i ) {
				var currentTile = tiles[i];
				var tileOffset = this.properties.mapData.tileOffset;
				var layerOffset = this.properties.mapData.layerOffset;

				var tileInfo = this.properties.tileData[currentTile.tileName];
				engineAssert( tileInfo, "Could not file tile info for tile name: " + currentTile.tileName );

				var posX = currentTile.col * tileOffset.x;
				var posY = currentTile.row * tileOffset.y + (this.properties.mapData.numLayers - currentTile.layer) * layerOffset;

				var tileProps = {
					assetName: tileInfo.assetName,
					x: posX,
					y: posY,
					z: currentTile.row + currentTile.layer,
					tileWidth: tileOffset.x,
					tileHeight: tileOffset.y,
					col: currentTile.col,
					row: currentTile.row
				};
				this.tiles.push( new Engine.MapTile(tileProps) );
				this.tileCount++;
			}
			this.bindEvent( "inserted", this, "inserted" );
		},

		inserted: function() {
			for( var i=0; i<this.tileCount; ++i ) {
				this.parentStage.insert( this.tiles[i] );
			}
		}
	});
};