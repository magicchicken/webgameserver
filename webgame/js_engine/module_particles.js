//*****************************************************************************
// Particles Module
//
// Required systems:
// * Class
// * Engine
// * Engine.Sprite
//
// Required libraries:
// * Underscore
//
// Features:
// * Engine.Particle
// * Engine.ParticleEmitterComponent
//*****************************************************************************

// Requires Engine namespace
(function() {
	engineAssert( Engine, "No Engine Definition" );
})();

//=========================================================================
// Particles Engine Module
//
//
//=========================================================================
Engine.Particles = function() {

	var DebugRadius = 10;
	var DebugFillStyle = "rgba(128, 0, 128, .4)";
	var DebugLineWidth = 1;
	var DebugStrokeStyle = "rgba(255, 0, 255, .4)";

	//=========================================================================
	// Particle class is a sprite
	// The ParticleEmitter is also our ParticleSystem, which means
	// it is in charge of rendering the particles it creates
	//
	//
	//
	//=========================================================================
	Engine.Particle = Engine.Sprite.extend({
		name: "Particle",

		defaults: {
			time: 0,
			lifeTime: 1
		}
	});

	//=========================================================================
	// ParticleEmitterComponent
	//
	// We make the emitter a component to attach it to any sprite.
	// While it is in charge of emitting particles, it also updates them
	// as if it were a Particle System.
	//
	// There is a lot of room for improvement on how we create and update particles.
	// Instead of using data from the properties, we could have functions
	// that generate the values based on an algorithm, or update them based
	// on some interpolation value
	//
	// Or, you can create a child class of this component, and override
	// its createParticle() and advanceParticle() functions.
	//
	// Or, you could make those two functions properties that get passed in to
	// the constructor for this emitter.
	//=========================================================================
	Engine.ParticleEmitterComponent = {

		name: "ParticleEmitterComponent",

		defaults: {
			// particle class to use
			particleClass: Engine.Particle,

			// emission parameters
			// Maybe you want to configure these to 'tune' the behavior of this
			// emitter. Or perhaps instead of being velocity driven, particles
			// are given a start and end position and we interpolate between them!
			particleAssetName: null,
			particleSheetName: null,
			
			spawnRate: 0.25,
			spawnRange: {x: 0, y: 0 },
			useVelocity: true,
			velocityRangeX: {min:-100, max:100},
			velocityRangeY: {min:-100, max:100},
			lifeTime: 3,

			// debug
			debugDraw: false
		},


		//=========================================================================
		// Constructor function
		//
		added: function(emitterProps) {
			// storage for active/alive particles
			this.activeParticles = [];
			this.deadParticles = [];
			this.spawnTimer = 0;

			this.entity.bindEvent("step", this, "step" );
			this.entity.bindEvent("draw", this, "draw" );
		},

		//=========================================================================
		// Creates a particle with the properties defined by this emitter
		//
		createParticle: function() {
			// particle starting position, somewhere within the defined box
			var posX = this.entity.properties.x + (Math.random() * this.properties.spawnRange.x) - (this.properties.spawnRange.x / 2);
			var posY = this.entity.properties.y + (Math.random() * this.properties.spawnRange.y) - (this.properties.spawnRange.y / 2);

			var props = {
				x: posX,
				y: posY,
				lifeTime: this.properties.lifeTime,
				assetName: this.properties.particleAssetName,
				sheetName: this.properties.particleSheetName
			};

			// particle velocity, anywhere within the velocity range
			if( this.properties.useVelocity ) {
				var totalRangeX = this.properties.velocityRangeX.max - this.properties.velocityRangeX.min;
				var totalRangeY = this.properties.velocityRangeY.max - this.properties.velocityRangeY.min;
				var dirX = this.properties.velocityRangeX.min + Math.random() * totalRangeX;
				var dirY = this.properties.velocityRangeY.min + Math.random() * totalRangeY;
				props.velocity = {
					x: dirX,
					y: dirY
				};
			}

			var newParticle = new this.properties.particleClass(props);
			this.activeParticles.push(newParticle);
			return newParticle;
		},

		//=========================================================================
		// Advanced all active particles with the properties defined by this emitter
		//
		advanceParticle: function(particle, dt) {
			// update particle properties based on elapsed time
			var p = particle.properties;
			p.time = Math.min( p.time + dt, p.lifeTime );

			// removes any dead particles from the list
			if( p.time >= p.lifeTime ) {
				this.deadParticles.push( particle );
			} else {
				if( p.velocity ) {
					p.x += p.velocity.x * dt;
					p.y += p.velocity.y * dt;
				}

				// Example of using an algorithm to drive property changes
				if( typeof this.properties.alpha == "function" ) {
					p.alpha = this.properties.alpha(p);
				}
			}
		},

		//=========================================================================
		// Updates active particles, removes dead particles, and creates
		// new particles based on emitter properties
		//
		step: function(dt) {
			// updates already created particles
			var particleCount = this.activeParticles.length;
			for( var i=0; i<this.activeParticles.length; ++i ) {
				this.advanceParticle( this.activeParticles[i], dt );
			}

			// destroy dead particles
			if(this.deadParticles.length > 0) {
				for(var i=0,len=this.deadParticles.length;i<len;i++) {
					var particle = this.deadParticles[i];
					var idx = _(this.activeParticles).indexOf(particle);
					this.activeParticles.splice(idx,1);
					if(particle.destroy) {
						particle.destroy();
					}
				}
				this.deadParticles.length = 0;
			}

			// creates any new particles based on emission properties
			var spawnCount = this.spawnTimer / this.properties.spawnRate;
			for( var index=0; index<spawnCount; ++index ) {
				this.createParticle();
				this.spawnTimer -= this.properties.spawnRate;
			}
			this.spawnTimer += dt;

		},

		//=========================================================================
		// Signals all of the particles that they are to draw
		// Also supports debug drawing features
		//
		draw: function(ctx) {
			for( var i=0; i<this.activeParticles.length; ++i ) {
				this.activeParticles[i].draw(ctx);
			}

			if( this.properties.debugDraw ) {
				if( this.properties.spawnRange.x === 0 && this.properties.spawnRange.y === 0 ) {
					this.engine.drawCircle( this.entity.properties.x, this.entity.properties.y, DebugRadius, DebugFillStyle, DebugLineWidth, DebugStrokeStyle);
				} else {
					var leftX = this.entity.properties.x - this.properties.spawnRange.x / 2;
					var topY = this.entity.properties.y - this.properties.spawnRange.y / 2;
					this.engine.drawRect( leftX, topY, this.properties.spawnRange.x, this.properties.spawnRange.y, DebugFillStyle, DebugLineWidth, DebugStrokeStyle );
				}
				this.engine.setFont( "normal 12px Arial" );
				this.engine.fillText( this.activeParticles.length + " particles", this.entity.properties.x, this.entity.properties.y, "black", "center");
			}
		},

		//=========================================================================
		// Destroys all of the particles
		//
		destroyed: function() {
			for( var i=0; i<this.activeParticles.length; ++i ) {
				this.activeParticles[i].destroy();
			}
			if(this.deadParticles.length > 0) {
				for(var i=0,len=this.deadParticles.length;i<len;i++) {
					this.deadParticles[i].destroy();
				}
			}
			this.activeParticles.length = 0;
			this.deadParticles.length = 0;
		}

	};

	//=========================================================================
	// Particles Module - Engine functionality
	//=========================================================================
	this.registerComponent( "particleEmitter", Engine.ParticleEmitterComponent );
};